﻿for (var i = 0; i < 10; i++) {
    console.log(i);
}

for (var i = 0; i < 10; i++) {
    if (i != 5) {
        console.log(i);
    }
}

for (var i = 0; i < 10; i++) {
    switch (i) {
        case 1:
        case 2:
        case 3:
        case 5:
        case 6:
        case 7:
        case 8:
            console.log(i);
            break;
    }
}

for (var i = 0; i < 10; i++) {
    switch (i) {
        case 0:
        case 4:
        case 9:
            break;
        default:
            console.log(i);
    }
}

for (var i = 0; i < 10; i++) {
    if (i != 0 && i != 4 && i != 9) {
        continue;
    }
    console.log(i);
}

for (var i = 0; i < 10; i++) {
    if (i % 2 == 0) {
        console.log(i);
    }
}

for (var i = 0; i < 10; i++) {
    if (i % 5 == 0) {
        console.log(i);
    }
}

// Example of changes
//Nog meer

// CHANGE 4.0
//Verandering 5.0

//Kopie toevoegen 5.1

for (var i = 0; i < 10; i++) {
    if (i % 2 == 0) {
        console.log(i);
    }
}

for (var i = 0; i < 10; i++) {
    if (i % 5 == 0) {
        console.log(i);
    }
}

// Kopie toegevoegd Versie 5.1.1

for (var i = 0; i < 10; i++) {
    switch (i) {
        case 0:
        case 4:
        case 9:
            break;
        default:
            console.log(i);
    }
}

// Kopie toegevoed Versie 6.0

for (var i = 0; i < 10; i++) {
    switch (i) {
        case 1:
        case 2:
        case 3:
        case 5:
        case 6:
        case 7:
        case 8:
            console.log(i);
            break;
    }
}

for (var i = 0; i < 10; i++) {
    switch (i) {
        case 0:
        case 4:
        case 9:
            break;
        default:
            console.log(i);
    }
}

// Versie 6.1

for (var i = 0; i < 10; i++) {
    switch (i) {
        case 1:
        case 2:
        case 3:
        case 5:
        case 6:
        case 7:
        case 8:
            console.log(i);
            break;
    }
}

for (var i = 0; i < 10; i++) {
    switch (i) {
        case 0:
        case 4:
        case 9:
            break;
        default:
            console.log(i);
    }
}